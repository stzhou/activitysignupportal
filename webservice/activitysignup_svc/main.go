package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/julienschmidt/httprouter"
	"github.com/tkanos/gonfig"
)

type Configuration struct {
	PORT       string
	MONGO      string
	HOSTURL    string
	MACHINEKEY string
	MAILSMTP   string
	MAILFROM   string
	MAILPWD    string
}

var CONFIG = Configuration{}

func main() {
	var isDebugMode bool = os.Args[0] == "debug"
	err := gonfig.GetConf("appconfig.json", &CONFIG)

	config2 := Configuration{}
	gonfig.GetConf("appconfig.json", &config2)

	var PORT string = CONFIG.PORT
	var mongoURL string = CONFIG.MONGO
	//setup log file
	fLog := openLogFile()
	log.SetOutput(fLog)
	defer fLog.Sync()
	defer fLog.Close()
	log.Printf("Application started at %v", time.Now())
	//initialize db
	var dbManager = DBManager{}
	err = dbManager.Connect(mongoURL, "actsignup")
	if err != nil {
		log.Fatal(err)
	}
	defer dbManager.Close()

	// webservice
	router := httprouter.New()

	router.GET("/api/activity", ApiDbContext(&dbManager, ActivityGetApi))
	router.POST("/api/activity", ApiDbContext(&dbManager, ActivityPostApi))

	router.POST("/api/signup", ApiDbContext(&dbManager, SignupPostApi))

	router.GET("/api/signupconfirmation/:token", ApiDbContext(&dbManager, SignupConfirmGetApi))

	//handle static files:
	staticDir := "static"

	//this depends on where the IDE put the debug binary,
	//assume it is in main.go directory
	if isDebugMode {
		staticDir = "../../dist/static"
	}

	router.ServeFiles("/static/*filepath", http.Dir(staticDir))

	if !isDebugMode {
		router.HandlerFunc("GET", "/", func(w http.ResponseWriter, r *http.Request) {
			http.Redirect(w, r, fmt.Sprintf("/%s", staticDir), 301)
		})
	}

	fmt.Printf("*** static files are served at directory %s\n", staticDir)
	fmt.Printf("Listening on port %s\n\n", PORT)
	log.Fatal(http.ListenAndServe(PORT, router))
}
