package main

import (
	"gopkg.in/mgo.v2/bson"
	"log"
	"strings"
)

type DBAccess struct {
	dbManager *DBManager
}

func (da *DBAccess) GetAllActivities() ([]Activity, error) {
	session, err := da.dbManager.Session()
	if err != nil {
		log.Printf("GetAllActivities() error %s\n", err.Error())
		return nil, err
	}
	c := session.DB(da.dbManager.dbName).C("activity")
	items := []Activity{}
	err = c.Find(nil).All(&items)
	if err != nil {
		log.Printf("GetAllActivities() error %s\n", err.Error())
		return nil, err
	}
	return items, nil
}

func (da *DBAccess) UpsertActivity(activity Activity) error {
	session, err := da.dbManager.Session()
	if err != nil {
		log.Printf("AddActivity() error %s\n", err)
		return err
	}
	c := session.DB(da.dbManager.dbName).C("activity")
	if err != nil {
		log.Printf("AddActivity() error %s\n", err)
		return err
	}
	if len(activity.Id) == 0 {
		err = c.Insert(&Activity{Name: activity.Name, Detail: activity.Detail})
	} else {
		err = c.UpdateId(activity.Id, activity)
	}
	if err != nil {
		log.Printf("AddActivity() insert error %s\n", err.Error())
	}
	return err
}

func (da *DBAccess) GetActivityById(activityHexId string) (*Activity, error) {
	session, err := da.dbManager.Session()
	if err != nil {
		log.Printf("GetActivityById() DB error %s\n", err)
		return nil, err
	}
	tbActivity := session.DB(da.dbManager.dbName).C("activity")
	activity := &Activity{}
	tbActivity.FindId(bson.ObjectIdHex(activityHexId)).One(&activity)
	return activity, nil
}

func (da *DBAccess) GetUserByEmail(email string) (*User, error) {
	session, err := da.dbManager.Session()
	if err != nil {
		log.Printf("GetUserByEmail db error %s\n", err.Error())
		return nil, err
	}
	tbUser := session.DB(da.dbManager.dbName).C("employee")
	email = strings.ToLower(email)
	user := &User{}
	tbUser.Find(bson.M{"email": email}).One(user)
	return user, nil
}

func (da *DBAccess) GetUserById(userHexId string) (*User, error) {
	session, err := da.dbManager.Session()
	if err != nil {
		log.Printf("GetUserByEmail db error %s\n", err.Error())
		return nil, err
	}
	tbUser := session.DB(da.dbManager.dbName).C("employee")
	user := &User{}
	tbUser.FindId(bson.ObjectIdHex(userHexId)).One(user)
	return user, nil
}

func (da *DBAccess) AddUser(firstName string, lastName string, email string) (*User, error) {
	session, err := da.dbManager.Session()
	if err != nil {
		log.Printf("Adduser db error %s\n", err.Error())
		return nil, err
	}
	tbUser := session.DB(da.dbManager.dbName).C("employee")
	user := &User{FirstName: firstName, LastName: lastName, Email: email}
	err = tbUser.Insert(user)
	if err != nil {
		log.Printf("AddUser db error %s\n", err.Error())
		return nil, err
	}
	return da.GetUserByEmail(user.Email)
}
