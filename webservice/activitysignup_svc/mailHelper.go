package main

import (
	"fmt"
	"log"
	"net/smtp"
	"strings"
)

type MailHelper struct {
	server string
	from   string
	pwd    string
}

// LoadConfig should be called before SendEmail
func (mh *MailHelper) LoadConfig() {
	mh.from = CONFIG.MAILFROM
	mh.server = CONFIG.MAILSMTP
	mh.pwd = CONFIG.MAILPWD
}

func (mh *MailHelper) Send(to string, subject string, content string) error {
	srv := strings.Split(mh.server, ":")
	auth := smtp.PlainAuth("", mh.from, mh.pwd, srv[0])
	msgPart := []string{}
	msgPart = append(msgPart, fmt.Sprintf("To: %s", to))
	msgPart = append(msgPart, fmt.Sprintf("Subject: %s", subject))
	msgPart = append(msgPart, content)
	msg := []byte(strings.Join(msgPart, "\r\n"))
	err := smtp.SendMail(mh.server, auth, mh.from, []string{to}, msg)
	if err != nil {
		log.Printf("Email Sending Failed with %s for %s\n", err.Error(), to)
		return err
	}
	log.Printf("Email has been sent to %s", to)
	return nil
}
