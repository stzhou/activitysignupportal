package main

import (
	"gopkg.in/mgo.v2/bson"
)

type SignUp struct {
	UserID    bson.ObjectId `bson:"_id,omitempty" json:"id"`
	Cmt       string        `json:"comment"`
	Confirmed bool          `json:"confirmed"`
}

type Activity struct {
	Id      bson.ObjectId `bson:"_id,omitempty" json:"id"`
	Name    string        `json:"name"`
	Detail  string        `json:"detail"`
	SignUps []SignUp      `json:"singups"`
}

type User struct {
	Id        bson.ObjectId `bson:"_id,omitempty" json:"id"`
	FirstName string        `json:"firstName"`
	LastName  string        `json:"lastName"`
	Email     string        `json:"email"`
}

//UserSignUp an api dto object.
type UserSignUp struct {
	FirstName  string `json:"firstName"`
	LastName   string `json:"lastName"`
	Email      string `json:"email"`
	Cmt        string `json:"comment"`
	ActivityId string `json:"activityId"`
}

type SignUpToken struct {
	UserID     string `json:"userID"`
	ActivityID string `json:"actID"`
}

type UsersAndAcitivity struct {
	Users      []User `json:"users"`
	Activity   string `json:"activity"`
	ActivityId string `json:"activityId"`
}
