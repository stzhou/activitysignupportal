package main

import (
	"gopkg.in/mgo.v2"
	"log"
)

type DBManager struct {
	session *mgo.Session
	connStr string
	dbName  string
}

func (m *DBManager) Connect(connStr string, dbName string) error {
	m.connStr = connStr
	m.dbName = dbName
	session, error := mgo.Dial("127.0.0.1:27017")
	if error != nil {
		log.Fatalln("Cannot connect to mongodb")
		return error
	} else {
		m.session = session
		return nil
	}
}
func (m *DBManager) Close() {
	if m.session != nil {
		m.Close()
	}
}

//Session pings db, and try to reestablish connection once.
func (m *DBManager) Session() (*mgo.Session, error) {
	err := m.session.Ping()
	if err == nil {
		return m.session, nil
	}
	log.Printf("failed to open db session %s", err.Error())

	//retry once:
	err = m.Connect(m.connStr, m.dbName)
	if err == nil {
		err = m.session.Ping()
	} else {
		log.Printf("failed to connect to db %s", err.Error())
		return nil, err
	}
	if err == nil {
		return m.session, nil
	}
	log.Printf("failed to connect to db %s", err.Error())
	return nil, err
}
