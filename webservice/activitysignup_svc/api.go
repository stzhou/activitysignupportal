package main

import (
	"encoding/json"
	"fmt"
	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2/bson"
	"log"
	"net/http"
	"strings"
)

type ApiDBContextHandlerFunc func(*DBManager, http.ResponseWriter, *http.Request, httprouter.Params)

func verifyJsonContent(r *http.Request) bool {
	contentType := r.Header.Get("Content-Type")
	return strings.ToLower(contentType) == "application/json"
}

func readJsonContent(r *http.Request, v interface{}) error {
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(v)
	defer r.Body.Close()
	if err != nil {
		log.Printf("readJsonContent %s error " + err.Error())
		return err
	}
	return nil
}

//ApiDbContext Middleware   exposes db to http handler
func ApiDbContext(m *DBManager, h ApiDBContextHandlerFunc) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		w.Header().Set("Content-Type", "application/json")
		h(m, w, r, p)
	}
}

//APIs
func ActivityGetApi(dbManager *DBManager, w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	dbAccess := &DBAccess{dbManager: dbManager}
	items, err := dbAccess.GetAllActivities()
	if err != nil {
		log.Printf("ActivityGetApi error %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	json, _ := json.Marshal(items)
	w.WriteHeader(http.StatusOK)
	w.Write(json)
}

func ActivityPostApi(dbManager *DBManager, w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	isJsonContent := verifyJsonContent(r)
	if !isJsonContent {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	activity := &Activity{}
	err := readJsonContent(r, activity)
	if err != nil {
		log.Printf("ReadActivity from content failed: %s", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	dbAccess := &DBAccess{dbManager: dbManager}
	err = dbAccess.UpsertActivity(*activity)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
}

func SignupPostApi(dbMng *DBManager, w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	//userid and comment are pushed to Activity entry.
	//Should prevent double signup.
	//user pushed signup can be confirmed only after link confirm.
	//everytime user signups the same activity, an new confirmation email will be sent.
	// (1) insert user if not there. (2) use userid to push to activity
	isJsonContent := verifyJsonContent(r)
	if !isJsonContent {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	userSignUp := &UserSignUp{}
	err := readJsonContent(r, userSignUp)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	//creat user if not there get userID.
	dbAccess := &DBAccess{dbManager: dbMng}
	user, err := dbAccess.GetUserByEmail(userSignUp.Email)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if user == nil {
		//create user:
		user, err = dbAccess.AddUser(userSignUp.FirstName, userSignUp.LastName, userSignUp.Email)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}

	activity, err := dbAccess.GetActivityById(userSignUp.ActivityId)
	if err != nil || activity == nil {
		log.Printf("SignupApi cannot find activity by ID %s \n", userSignUp.ActivityId)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	//potential bug: when this thread is reading the list another thread adds the user to db.
	foundSignedup := false
	for _, s := range activity.SignUps {
		if s.UserID == user.Id {
			foundSignedup = true
		}
	}
	sendConfirmEmail := func(activityName string, to string, token SignUpToken) {
		mailhelper := &MailHelper{}
		mailhelper.LoadConfig()
		tokenStr, err := createSignUpTokenString(token)
		if err != nil {
			log.Printf("Cannot send to user confirmation email, token generation failure: %s", to)
			return
		}
		tokenURL := `%s%s/api/signupconfirmation/%s`
		tokenURL = fmt.Sprintf(tokenURL, CONFIG.HOSTURL, CONFIG.PORT, tokenStr)

		mailhelper.Send(to, "Activity Signup Confirmation", fmt.Sprintf("Thank you for signup activity %s in our system.\r\n Please Click %s to confirm", activityName, tokenURL))
	}
	if !foundSignedup {
		activity.SignUps = append(activity.SignUps, SignUp{UserID: user.Id, Cmt: userSignUp.Cmt, Confirmed: false})
		err = dbAccess.UpsertActivity(*activity)
	} else {
		// user already signed up, still return all signed up docs:
		// re-send email notification and link for confirmation
	}
	if err != nil {
		log.Printf("activity signup insert failed %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
	} else {

		go sendConfirmEmail(activity.Name, user.Email, SignUpToken{UserID: user.Id.Hex(), ActivityID: activity.Id.Hex()})

		w.WriteHeader(http.StatusCreated)

		signedupUsers := []User{}
		for _, s := range activity.SignUps {
			user, err := dbAccess.GetUserById(s.UserID.Hex())
			if err != nil {
				continue
			}
			user.Email = "N/A"          //remove email
			user.Id = bson.ObjectId("") //remove id;
			if len(user.Email) > 0 && s.Confirmed {
				signedupUsers = append(signedupUsers, *user)
			}
		}
		usersAndActs := &UsersAndAcitivity{Users: signedupUsers, Activity: activity.Name, ActivityId: activity.Id.Hex()}
		json, _ := json.Marshal(usersAndActs)
		w.Write(json)
	}
}

func SignupConfirmGetApi(dbMng *DBManager, w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	confirmToken := p.ByName("token")
	w.Header().Set("Content-Type", "text/plain")
	signupToken, err := unmarshalSignupTokenString(confirmToken)
	if err != nil {
		fmt.Fprintf(w, "Sorry, we can not confirm your signup, token is not valid")
		return
	}
	if err != nil {
		fmt.Fprintf(w, "Sorry, we can not confirm your signup, server error")
		return
	}
	dbAccess := &DBAccess{dbManager: dbMng}
	activity, err := dbAccess.GetActivityById(signupToken.ActivityID)
	if err != nil {
		fmt.Fprintf(w, "Sorry, we can not confirm your signup, token is not valid")
		log.Printf("SignupConfirmGetApi activity invalid from signupToken %v \n", signupToken)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	shouldUpdate := false
	for i, s := range activity.SignUps {
		if s.UserID == bson.ObjectIdHex(signupToken.UserID) {
			activity.SignUps[i].Confirmed = true
			shouldUpdate = true
		}
	}
	if shouldUpdate {
		err := dbAccess.UpsertActivity(*activity)
		if err != nil {
			log.Printf("SignupConfirmGetApi, UpdateDB error: %s", err.Error())
			fmt.Fprintf(w, "Sorry, we can not confirm your signup, server error")
		} else {
			fmt.Fprintf(w, "Your singup is confirmed, thank you!")
		}
	}
}
