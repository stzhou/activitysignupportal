package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"
	"time"
)

// openLogFile creates log file in go binary directory, each restart gets a new timestamp in name
func openLogFile() *os.File {
	fileTemplate := "activity_signup_%s.log"
	curUtcTime := time.Now().UTC().Format(time.RFC3339)
	//it return UTC time safe for file name: 1999-01-01T12-21-23Z
	curUtcTime = strings.Replace(curUtcTime, ":", "-", 3)

	f, err := os.OpenFile(fmt.Sprintf(fileTemplate, curUtcTime), os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)

	if err == nil {
		return f
	}
	panic("Cann't open log file! %s \n" + err.Error())
}

// createSignUpTokenString AES encrypted json object
func createSignUpTokenString(token SignUpToken) (string, error) {
	bytes, _ := json.Marshal(token)
	helper := &EncryptionHelper{}
	ret, err := helper.encryptToURLSafeString(CONFIG.MACHINEKEY, bytes)
	if err != nil {
		log.Printf("createSignUpTokenString failed: %s", err.Error())
		return "", nil
	}
	return ret, nil
}

func unmarshalSignupTokenString(crypto string) (SignUpToken, error) {
	helper := &EncryptionHelper{}
	JSON, err := helper.decryptFromURLSafeSecret(CONFIG.MACHINEKEY, crypto)
	token := &SignUpToken{}
	if err != nil {
		log.Printf("unmarshalSignupTokenString failed: %s", err.Error())
		return *token, err
	}
	err = json.Unmarshal(JSON, &token)
	return *token, err
}
