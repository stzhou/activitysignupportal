import { Component, OnInit } from '@angular/core';
import { DatastoreService } from "./datastore.service"
import { Router } from '@angular/router';
import * as Model from "./datastore.service";

@Component({
  selector: 'app-activity-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activity-list.component.css']
})
export class ActivityListComponent implements OnInit {

  title = 'app';
  activityList: Array<Model.Activity>;
  detailId:string;

  constructor( private ds: DatastoreService, private router:Router){
  }

  ngOnInit(){
    this.ds.getActivityList().then((data:Array<Model.Activity>)=>{
      this.activityList = data;
    })
  }

  signup = (id:string, event:Event) => {
    event.stopPropagation();
    event.preventDefault();
    this.router.navigate(['/signup', {id:id}]);
  }

  showDetail = (id: string, event: Event)=> {
    event.preventDefault();
    event.stopPropagation();
    if (this.detailId === id) {
      this.detailId = null;
    } else {
      this.detailId = id;
    }
    return false;
  }
}
