import { Injectable } from '@angular/core';
import { NgModule } from '@angular/core/src/metadata/ng_module';
import {HttpClient} from "@angular/common/http"
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import * as Q from 'q';

@Injectable()
export class DatastoreService implements OnInit {
  constructor(private http: HttpClient) { 

  }
  ngOnInit():void {
    this.http.get('/api/activity').subscribe((data)=>{
      console.log(data)
    })
  }
  getActivityList = () => {
     return  this.http.get<Array<Activity>>('/api/activity')
     .toPromise().then((data)=>{
       const activities:Array<Activity>=[];
        if(data.length){
          data.forEach((p)=>{
            const _act: Activity = {id:p.id, name:p.name, detail:p.detail}
            activities.push(_act);
          })
        }
        return activities.sort(function(a: any,b:any){return (a.name - b.name)});
     })
  }

  private formatUsers(users:Array<User>): Array<User>{
    if (!users || !users.length) return [];
    users.forEach((u) => {
      u.firstName = u.firstName.toUpperCase();
      u.lastName = u.lastName.toUpperCase();
    });
    return users.sort(function (a: any, b: any) {
      return (a.firstName + a.lastName) as any - (b.firstName + b.lastName) as any
    });
  }

  saveSignup = (signup: Signup) => {
    const qResults = Q.defer<UserActivity>();
    this.http.post('/api/signup', signup).toPromise()
      .then((res: UserActivity) => {
        res.users = this.formatUsers(res.users);
        qResults.resolve(res);
      })
      .catch((err) => qResults.reject(err));
    return qResults.promise;
  }
  createActivity = (name :string, detail :string)=>{
    return this.http.post("/api/activity", {name:name, detail:detail}).toPromise();
  }
}
export interface Activity {
  id: number
  name: string
  detail: string
}
export interface Signup {
  firstName: string
  lastName: string
  email: string
  comment: string
  activityId: string
}
export interface User {
  firstName: string
  lastName:string
}
export interface UserActivity {
  activity:string,
  activityId:string,
  users:Array<User>
}
