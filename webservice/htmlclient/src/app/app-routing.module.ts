import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SignupFormComponent } from "./signup-form.component"
import { ActivityListComponent } from "./activity-list.component"
import { CreateActivityComponent } from "./create-activity.component"
import { AppComponent } from "./app.component"

const routes: Routes =[
  { path: "signup", component: SignupFormComponent },
  { path: "activity", component: ActivityListComponent },
  { path: "create", component: CreateActivityComponent },
  { path: "", redirectTo: 'activity', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports:[ RouterModule ]
})
export class AppRoutingModule { }
