import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';

import { AppComponent } from './app.component';

import {HttpClientModule} from "@angular/common/http";
import {DatastoreService} from "./datastore.service";
import { AppRoutingModule } from './app-routing.module';
import { SignupFormComponent } from "./signup-form.component";
import { ActivityListComponent } from './activity-list.component';
import { CreateActivityComponent } from './create-activity.component';

@NgModule({
  declarations: [
    AppComponent,
    SignupFormComponent,
    ActivityListComponent,
    CreateActivityComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [DatastoreService],
  bootstrap: [AppComponent]
})
export class AppModule { }
