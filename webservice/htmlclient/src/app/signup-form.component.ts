import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as Model from "./datastore.service"
import {DatastoreService } from "./datastore.service"

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.css']
})
export class SignupFormComponent implements OnInit {

  constructor(private route: ActivatedRoute, private ds: DatastoreService, private ngZone: NgZone) {
    this.signup = { firstName: '', lastName: '', activityId: '', email: '', comment: '' };
  }
  activityId: string;
  activity:string;
  signup :Model.Signup;
  signupUsers: Array<Model.User>;
  isDone: boolean;
  ngOnInit() {
    this.route.params.subscribe((p: { id: string }) => {
      this.activityId = p.id;
      this.signup.activityId = this.activityId;
    })
  }
  onSignupDown = (res:Model.UserActivity)=>{
    this.isDone = true;
    this.signupUsers = res.users;
    this.activity = res.activity;
    if (this.activityId != res.activityId) {
      console.error("Api return incorrect activity.");
    }
  }
  validateSubmit = () => {
    if (!this.signup.activityId) return false;
    if( !this.signup.firstName || !this.signup.firstName.trim()) return false;
    if( !this.signup.lastName|| !this.signup.lastName.trim()) return false;
    if( !this.signup.email || this.signup.email.indexOf('@')===-1 ) return false;
    return true;
  }

  signupSubmit = (event: Event) => {
    event.preventDefault();
    if (!this.validateSubmit()) {
      alert("Form Validation failed. Please make sure you information is not empty.");
      return;
    }

    this.ds.saveSignup(this.signup).then((res) => {
      this.ngZone.run(() => {
        this.onSignupDown(res);
      });
    }).catch((err) => {
      alert("It is embarrassing, something is wrong.")
    });
  }
  get diagnostic(){
    return JSON.stringify(this.signup);
  }
}

