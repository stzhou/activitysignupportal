import { Component, OnInit } from '@angular/core';
import { DatastoreService } from "./datastore.service"
import * as Model from "./datastore.service"

@Component({
  selector: 'app-create-activity',
  templateUrl: './create-activity.component.html',
  styleUrls: ['./create-activity.component.css']
})
export class CreateActivityComponent implements OnInit {

  constructor(private ds: DatastoreService) { }
  activities: Array<Model.Activity>;
  newActivityName: string;
  newActivityDetail: string;

  ngOnInit() {
    this.ds.getActivityList().then((res)=>{
      this.activities = res;
    });
  }
  createActivity=()=>{
    if (this.newActivityName) {
      this.ds.createActivity(this.newActivityName, this.newActivityDetail)
        .then(() => {
          this.ngOnInit();
        })
        .catch((err) => console.error(err));
    }
  }
}
