## Description
This portal is a 2-days project, for collecting user signups of activities.
It doesn't have user/password authentication due to its project time-line, however it uses email confirmation to improve its usefulness.

### Features
- It is designed to work on mobile phone screen.
- When user open default page, a list of activites is displayed. Clicking the list or '...' button, it will display the detail of the activity.
- User clicks the list item, will go to Signup page, they will need to enter signup information and optional signup comment for selected activity.
- User's email as the unique identifier allows user to signup activity once, but confirmation link will be sent to given email when Signup is submitted all the time.
- Confirmation link is an encrypted json object recorded information that the server api can use to mark the signup itself as 'confirmed', else, the signup will not be listed.
- After signup submit, the user will be able to see the list of confirmed Signups.
- (To create activity, a 'tool' page is on http://localhost:8080/static/#/create, this is not navigated from anywhere, since it is a data entry tool).

### Technical Choices
- Mongodb via docker. For small project and data, it is as fast as memory cache, no need to worry about schema deployment.
- Golang. Very simple dev environment setup on linux, mac, windows. Cross platform compilation. Single file deployment. No need for .net or any VM runtime. Very fast. Very clean and simple language structure and the framework.
- Angular4: Using ECMA latest, typescript. Well structured, fully featured and opnionated framework. Compared to ReactJS, it doesn't have headaches when dealing with events and two-way data flow, good for large projects where size > 2MB is not a problem.

## golang global install
Make sure your go bin/ folder is in your environment.

```
export PATH=$PATH:$GOPATH/bin
```

install govendor
```
go get -u github.com/kardianos/govendor
```

## mongo db via docker / docker hub
install mongo to docker:
mkdir -p $HOME/dockerScripts/actsign
cd  $HOME/dockerScripts/actsign
docker pull mongo:3.6.0


**Mongodb should bind to localhost:port to avoid exposing access to network, or should use authentication + TLS/SSH.**

create&start mongodb from docker:

```bash
docker run --name actsign-mongo -v $(pwd)/data:/data/db -p 127.0.0.1:27017:27017 -d mongo:3.6.0
```

restart database container if reboot

```
docker start actsign-mongo
```

## build project
Please use GNU make tool. read Makefile for detailed options

### build golang
To build go service project and copy config and binary to dist folder:
(go project has no dev built, to debug use IDE's go debug tool gdb or dlv)
```bash
make service
```

### build angular4
To build angular4 application for developement and run "angular serve"

```bash
make debug
```

To build angular4 and copy to dist/ folder.

```bash
make static
```


## Mongo Doc/Schema Example
the scheme is defined in Go project's model.go


To avoid implementing password login for this small project, email confirmation is used to make sure no one can abuse the system's subscription data.
Password login requires complexity, forget-password, basic authentication etc, too much for this purpose.

+ db.activity
```javascript
//mongoshell> db.activity.find()
{ "_id" : ObjectId("5a31e670867844a09abef544"), "name" : "Activity 1", "detail" : "Some Activity details.", "signups" : [ { "_id" : ObjectId("5a320edb867844a09abef6f5"), "cmt" : "my comment", "confirmed" : false }, { "_id" : ObjectId("5a3303d5c47481af522cbd27"), "cmt" : "my comment", "confirmed" : true } ] }
{ "_id" : ObjectId("5a31e689867844a09abef54c"), "name" : "Activity 2", "detail" : "Some Activity details.", "signups" : [ ] }
```
+ db.employee
```javascript
//mongoshell> db.employee.find()
{ "_id" : ObjectId("5a320edb867844a09abef6f5"), "firstname" : "sherman", "lastname" : "z", "email" : "sherman.test@123.com" }
{ "_id" : ObjectId("5a3303d5c47481af522cbd27"), "firstname" : "sherman", "lastname" : "z", "email" : "shermantzhou@gmail.com" }
```