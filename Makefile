ROOT =  ActivitySignupPortal
SVCPATH = $(ROOT)/webservice
configFile = appconfig.json

service:
		#govendor will build into project directory
		govendor build $(SVCPATH)/activitysignup_svc
		#copy dev build to dist folder.
		cp activitysignup_svc dist/

		#Copy appconfig.json if doesn't exist
		if [ ! -f ${GOPATH}/src/$(ROOT)/dist/$(configFile) ]; then \
			cp ${GOPATH}/src/$(SVCPATH)/activitysignup_svc/appconfig.json ${GOPATH}/src/$(ROOT)/dist/ ;\
		fi;

windows:
	    GOOS=windows GOARCH=amd64 govendor build $(SVCPATH)/activitysignup_svc
		cp activitysignup_svc.exe dist/

debug:
		# starts angular debug server, proxy to golang server 8080 for /api/
		# requires angular cli
		cd webservice/htmlclient; ng serve --proxy-config proxy.conf.json;
static:
		# requires angular cli
		cd webservice/htmlclient; ng build --base-href /static/;

.PHONY: clean
clean:
		rm -r dist/*


